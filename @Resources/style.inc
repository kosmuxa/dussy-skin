[ClockStyle]
	AntiAlias			= 1
	Fontcolor			= F1F1F1
	FontFace			= "Exo 2 Extra Light"
	FontSize			= 60
[DateStyle]
	AntiAlias			= 1
	Fontcolor 			= F1F1F1
	FontFace 			= "Exo 2"
	FontSize 			= 20
	InlineSetting 		= CharacterSpacing | 3 | 4
	InlinePattern		= .*
	InlineSetting2		= Weight | 200
	InlinePattern2		= .*
[ClockStyleWhite]
	AntiAlias			= 1
	Fontcolor			= 0E0E0E
	FontFace			= "Exo 2 Extra Light"
	FontSize			= 60
[DateStyleWhite]
	AntiAlias			= 1
	Fontcolor 			= 0E0E0E
	FontFace 			= "Exo 2"
	FontSize 			= 20
	InlineSetting 		= CharacterSpacing | 3 | 4
	InlinePattern		= .*
	InlineSetting2		= Weight | 200
	InlinePattern2		= .*

[PlayerTitleStyle]
	AntiAlias 			= 1
	FontColor 			= F1F1F1
	FontFace 			= "Exo 2"
	FontSize 			= 14
	InlineSetting		= Weight | 300
	InlinePattern		= .*
[PlayerArtistStyle]
	AntiAlias 			= 1
	FontColor 			= F1F1F1
	FontFace 			= "Exo 2"
	FontSize 			= 12
	InlineSetting		= Weight | 200
	InlinePattern		= .*
[PlayerTimeStyle]
	AntiAlias 			= 1
	FontColor 			= B1B1B1
	FontFace 			= "Exo 2"
	FontSize 			= 9
	InlineSetting		= Weight | 200
	InlinePattern		= .*

[LauncherIconStyle]
	H 					= 100
	W 					= 100
	Y 					= 25

[HiderTextStyle]
	AntiAlias 			= 1
	FontColor 			= F1F1F1
	FontFace 			= "Exo 2"
	FontSize 			= 14
	InlineSetting		= Weight | 500
	InlinePattern		= .*
	